/*
 * CSRNG (Cryptographically secure random number generator) based on HMAC-SHA512 algorithm
 * Created by LoRd_MuldeR <mulder2@gmx.de>.
 * Please visit the official web-site at http://muldersoft.com/ for news and updates!
 * 
 * This work is licensed under the CC0 1.0 Universal License.
 * To view a copy of the license, visit:
 * https://creativecommons.org/publicdomain/zero/1.0/legalcode
 */

package com.muldersoft.hmacsha512csrng.test;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import com.muldersoft.hmacsha512csrng.HmacSha512Csrng;

import gnu.trove.set.hash.TLongHashSet;

public class HmacSha512CsrngTest {

	private static final int BATCH_SIZE = 4096;
	
	/**
	 * Main function
	 */
	public static void main(String[] args) {
		// Print logo
		System.err.printf("HMAC-SHA512-based CSRNG v%d.%02d, created by LoRd_MuldeR <mulder2@gmx.de>.\n",
				HmacSha512Csrng.VERSION_MAJOR, HmacSha512Csrng.VERSION_MINOR);
		System.err.println("This work is licensed under the CC0 1.0 Universal License.\n");
		
		// Determine number of CPUs
		final int processorCount = getNumberOfProcessors();

		// Set up histogram
		final AtomicLong[][] histogram = new AtomicLong[8][];
		for(int i = 0; i < 8; ++i) {
			histogram[i] = new AtomicLong[256];
			for(int j = 0; j < 256; ++j) {
				histogram[i][j] = new AtomicLong(0L);
			}
		}

		// Main processing loop
		double ratio = Double.MIN_VALUE;
		do {
			// Run the test!
			if(!runTest(processorCount, histogram)) {
				break; /*failure*/
			}

			// Find minimum and maximum occurrences
			System.out.println("\n---------------------\n");
			ratio = dumpHistogram(histogram);
			System.out.println("\n---------------------\n");
			
			// Small delay
			try {
				Thread.sleep(1500);
			} catch (final InterruptedException e) {
				break; /*interrupted*/
			}
			
			// Force clean-up
			Runtime.getRuntime().gc();
		} while(ratio < 0.999);
	}

	/**
	 * Test function
	 */
	private static boolean runTest(final int threadCount, final AtomicLong[][] histogram) {
		// Allocate variables
		final Thread[] threads = new Thread[threadCount];
		final AtomicBoolean stopFlag = new AtomicBoolean(false);
		final TLongHashSet hashSet = new TLongHashSet();
		
		// Set up the worker threads
		for(int i = 0; i < threadCount; ++i) {
			threads[i] = new Thread() {
				@Override
				public void run() {
					long count = 0;
					final HmacSha512Csrng hmacSha512Csrng = HmacSha512Csrng.getInstance();
					try {
						while(!stopFlag.get()) {
							long[] values = hmacSha512Csrng.nextLongs(BATCH_SIZE);
							for(int i = 0; i < BATCH_SIZE; ++i) {
								histogram[0][(int)((values[i] >>>  0) & 0xFF)].incrementAndGet();
								histogram[1][(int)((values[i] >>>  8) & 0xFF)].incrementAndGet();
								histogram[2][(int)((values[i] >>> 16) & 0xFF)].incrementAndGet();
								histogram[3][(int)((values[i] >>> 24) & 0xFF)].incrementAndGet();
								histogram[4][(int)((values[i] >>> 32) & 0xFF)].incrementAndGet();
								histogram[5][(int)((values[i] >>> 40) & 0xFF)].incrementAndGet();
								histogram[6][(int)((values[i] >>> 48) & 0xFF)].incrementAndGet();
								histogram[7][(int)((values[i] >>> 56) & 0xFF)].incrementAndGet();
							}
							synchronized(hashSet) {
								for(int i = 0; i < BATCH_SIZE; ++i) {
									if(!hashSet.add(values[i])) {
										throw new RuntimeException("Collision detected!");
									}
								}
								count = hashSet.size();
							}
							System.out.printf("[%010d] %016X\n", count, values[BATCH_SIZE-1]);
						}
					} catch (final Throwable e) {
						if(stopFlag.compareAndSet(false, true)) {
							Thread.yield();
							try {
								System.err.println(e.getClass().getName() + ": " + e.getMessage());
							} catch(final Throwable e2) { }
						}
					}
				}
			};
		}
		
		// Start the threads!
		for(int i = 0; i < threadCount; ++i) {
			threads[i].start();
		}
		
		// Wait for completion
		for(int i = 0; i < threadCount; ++i) {
			try {
				threads[i].join();
			} catch (final InterruptedException e) {
				return false; /*interrupted*/
			}
		}
		
		return true;
	}
	
	/**
	 * Print histogram and get smallest ratio
	 */
	private static double dumpHistogram(final AtomicLong[][] histogram) {
		double globalMin = Double.MAX_VALUE;
		for(int i = 0; i < 8; ++i) {
			long min = Long.MAX_VALUE, max = Long.MIN_VALUE;
			for(int j = 0; j < 256; ++j) {
				final long value = histogram[i][j].get();
				min = Math.min(min, value);
				max = Math.max(max, value);
			}
			final double ratio = ((double)min) / ((double)max);
			System.out.printf("[%d] %016X %016X -> %7.5f\n", i, min, max, ratio);
			globalMin = Math.min(globalMin, ratio);
		}
		return globalMin;
	}
	
	/**
	 * Detect the number of available processors
	 */
	private static int getNumberOfProcessors() {
		try {
			final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
			return Math.max(1, operatingSystemMXBean.getAvailableProcessors());
		} catch (final Exception e) {
			return 1;
		}
	}
}
