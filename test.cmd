@echo off

if not exist "%JAVA_HOME%\bin\java.exe" (
	echo Java could not be found. Please check your JAVA_HOME and try again!
	pause
	goto:eof
)

@echo on
"%JAVA_HOME%\bin\java.exe" -server -jar "%~dp0.\out\hmacsha512csrng-1.jar" - | "%~dp0..\Prerequisites\Dieharder\bin\dieharder.exe" -a -k 2 -Y 1 -g 200
@echo off

pause
